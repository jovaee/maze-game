﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Objects;

namespace Maze_Game {
	public partial class GameWindow : Form {

		private Painter _painter;

		private int _mouseX;
		private int _mouseY;

		private int _mouseButton;

		private Timer _timer;

		public GameWindow() {
			InitializeComponent();

			Generator.ShowGeneration = false;

			_timer = new Timer();
			_timer.Interval = 40;
			_timer.Tick += new EventHandler(TimerAction);
		}

		private void test(object sender, EventArgs e) {
			GamePanel.BackColor = Color.LightBlue;
		}

		private void GameWindow_Load(object sender, EventArgs e) {
			_painter = new Painter(this.GamePanel);

			backgroundWorker1.RunWorkerAsync();
			
		}

		private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e) {
			_painter.Clear();
			Generator.GenerateMaze(20, _painter);
		}

		private void GamePanel_MouseDown(object sender, MouseEventArgs e) {
			/*Graphics g = GamePanel.CreateGraphics();

			g.FillRectangle(Brushes.Red, new Rectangle(_mouseX, _mouseY, 12, 12));
			g.Dispose();*/

			if (e.Button == MouseButtons.Left) {
				_mouseButton = 0;
			} else if (e.Button == MouseButtons.Right) {
				_mouseButton = 1;
			}

			_timer.Start();
		}

		private void GamePanel_MouseMove(object sender, MouseEventArgs e) {
			_mouseX = e.X;
			_mouseY = e.Y;
		}

		private void GamePanel_MouseUp(object sender, MouseEventArgs e) {
			_timer.Stop();
		}

		private void TimerAction(object sender, EventArgs e) {
			Graphics g = GamePanel.CreateGraphics();

			if (_mouseButton == 0) {
				g.FillEllipse(Brushes.Red, new Rectangle(_mouseX, _mouseY, 8, 8));
			} else if (_mouseButton == 1) {
				g.FillEllipse(Brushes.White, new Rectangle(_mouseX, _mouseY, 8, 8));
			}

			g.Dispose();
		}

		private void GamePanel_MouseClick(object sender, MouseEventArgs e) {
			if (e.Button == MouseButtons.Middle) {
				_painter.Clear();
				_painter.CleanPaint(Generator.maze);
			}
		}

		private void button1_Click(object sender, EventArgs e) {
			if (!backgroundWorker1.IsBusy) {
				backgroundWorker1.RunWorkerAsync();
			} else {
				Console.WriteLine("========== Busy Generating Maze Already ==========");
			}			
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e) {
			Generator.ShowGeneration = checkBox1.Checked;
		}
	}

	public class Painter {

		private Panel _gp;

		public Painter(Panel gp) {
			_gp = gp;
		}

		public void Clear() {
			Graphics g = _gp.CreateGraphics();

			g.Clear(Color.White);
			g.Dispose();
		}

		public void CleanPaint(Cell[,] maze) {
			int x = 25, y = 25;
			int height = 25, width = 25;

			Graphics g = _gp.CreateGraphics();

			for (int i = 0; i < maze.GetLength(1); i = i + 1) {
				for (int j = 0; j < maze.GetLength(1); j = j + 1) {
					if (maze[i, j].PartOfMaze) {
						g.FillRectangle(Brushes.White, new Rectangle(x, y, width, height));

						if (maze[i, j].Left.Build) {
							g.DrawLine(Pens.Blue, new Point(x, y), new Point(x, y + height));
						}

						if (maze[i, j].Right.Build) {
							g.DrawLine(Pens.Blue, new Point(x + width, y), new Point(x + width, y + height));
						}

						if (maze[i, j].Top.Build) {
							g.DrawLine(Pens.Blue, new Point(x, y), new Point(x + width, y));
						}

						if (maze[i, j].Bottom.Build) {
							g.DrawLine(Pens.Blue, new Point(x, y + height), new Point(x + width, y + height));
						}
					}

					x = x + width;
				}

				y = y + height;
				x = 25;
			}

			g.Flush();
			g.Dispose();
		}

		public void Paint(Cell[,] maze, int k, int l) {
			int x = 25, y = 25;
			int height = 25, width = 25;

			Graphics g = _gp.CreateGraphics();			
		
			for (int i = 0; i < maze.GetLength(1); i = i + 1) {
				for (int j = 0; j < maze.GetLength(1); j = j + 1) {
					if (maze[i, j].PartOfMaze) {
						g.FillRectangle(Brushes.White, new Rectangle(x, y, width, height));

						if (maze[i,j].Left.Build) {
							g.DrawLine(Pens.Blue, new Point(x, y), new Point(x, y + height));
						}

						if (maze[i, j].Right.Build) {
							g.DrawLine(Pens.Blue, new Point(x + width, y), new Point(x + width, y + height));
						}

						if (maze[i, j].Top.Build) {
							g.DrawLine(Pens.Blue, new Point(x, y), new Point(x + width, y));
						}

						if (maze[i, j].Bottom.Build) {
							g.DrawLine(Pens.Blue, new Point(x, y + height), new Point(x + width, y + height));
						}
					}

					if (k == i && l == j) {
						g.FillRectangle(Brushes.Red, new Rectangle(x+2, y+2, width-2, height-2));
					}
					
					x = x + width;
				}

				y = y + height;
				x = 25;
			}

			g.Flush();
			g.Dispose();
		}
	}
}
