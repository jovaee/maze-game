﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Objects;
using System.Threading;

namespace Maze_Game {
	public static class Generator {

		public static Cell[,] maze;
		private static int _size;

		public static bool ShowGeneration {get; set;}

		private static Painter _painter;

		/// <summary>
		/// Generate a maze with specified size
		/// </summary>
		/// <param name="size"></param>
		public static void GenerateMaze(int size, Painter painter) {
			_size = size;
			_painter = painter;

			CreateMaze();
			GenerateMaze();
		}

		private static void CreateMaze() {
			Console.WriteLine("Create Maze Array");
			maze = new Cell[_size, _size];

			for (int i = 0; i < maze.GetLength(1); i = i + 1) {
				for (int j = 0; j < maze.GetLength(1); j = j + 1) {
					maze[i, j] = new Cell(i, j);
				}
			}

			Console.WriteLine("Created Maze Array");
		}

		private static void GenerateMazeV2() {
			Console.WriteLine("Generating Maze");

			Random random = new Random();

			List<Cell> unvisitedCells = new List<Cell>();
			List<Cell> tmp = new List<Cell>();
			Queue<Cell> queue = new Queue<Cell>();

			// Add all unvisited cells
			for (int i = 0; i < maze.GetLength(1); i = i + 1) {
				for (int j = 0; j < maze.GetLength(1); j = j + 1) {
					unvisitedCells.Add(maze[i, j]);
				}
			}

			Cell currentCell = maze[0, 0], chosenCell;
			currentCell.PartOfMaze = true;
			unvisitedCells.Remove(currentCell);

			while (unvisitedCells.Count > 0) {
				//Left
				if (currentCell.i > 0 && !maze[currentCell.i - 1, currentCell.j].PartOfMaze) {
					tmp.Add(maze[currentCell.i - 1, currentCell.j]);
				}

				//Right
				if (currentCell.i < _size - 1 && !maze[currentCell.i + 1, currentCell.j].PartOfMaze) {
					tmp.Add(maze[currentCell.i + 1, currentCell.j]);
				}

				//Top
				if(currentCell.j > 0 && !maze[currentCell.i, currentCell.j - 1].PartOfMaze) {
					tmp.Add(maze[currentCell.i, currentCell.j - 1]);
				}

				//Bottom
				if (currentCell.j < _size - 1 && !maze[currentCell.i, currentCell.j + 1].PartOfMaze) {
					tmp.Add(maze[currentCell.i, currentCell.j + 1]);
				}

				if (tmp.Count > 0) {
					chosenCell = tmp.ElementAt(random.Next(0, tmp.Count));
					queue.Enqueue(currentCell);

					//Remove the wall between two cells
					/*if () {

					} else if () {

					} else if () {

					} else if () {

					}*/

					currentCell = chosenCell;
					currentCell.PartOfMaze = true;

					tmp.Clear();

					_painter.Paint(maze, currentCell.i, currentCell.j);

					//Make the thread sleep so that the user can see the move taken
					Thread.Sleep(125);
				} else if (queue.Count > 0) {
					currentCell = queue.Dequeue();
				}

				
			}			

			Console.WriteLine("Generated Maze");
		}

		private static void GenerateMaze() {
			Console.WriteLine("Generating Maze");

			List<Wall> walls = new List<Wall>();
			Wall selectedWall;

			Random random = new Random();
			int direction, ran;

			bool added = false;

			// Pick the cell at (0,0) as the starting point
			Cell cell = maze[0, 0], otherCell = null;
			cell.PartOfMaze = true;

			walls.Add(cell.Left);
			walls.Add(cell.Right);
			walls.Add(cell.Top);
			walls.Add(cell.Bottom);

			while (walls.Count > 0) {
				selectedWall = walls.ElementAt(ran = random.Next(0, walls.Count));			

				direction = random.Next(0, 4);

				added = false;

				// 0: Left
				// 1: Right
				// 2: Top
				// 3: Bottom			

				if (direction == 0) {
					//selectedWall.Build = false;
					cell = selectedWall.ParentCell;

					// Check if the cell in that direction is not already part of maze
					if (cell.i > 0 && !maze[cell.i - 1, cell.j].PartOfMaze) {

						cell.Left.Build = false;

						// Add all the walls of the neighboring cell;
						otherCell = maze[cell.i - 1, cell.j];
						otherCell.Right.Build = false;

						walls.Add(otherCell.Left);
						//walls.Add(otherCell.Right);
						walls.Add(otherCell.Top);
						walls.Add(otherCell.Bottom);

						otherCell.PartOfMaze = true;
						added = true;
					}
				} else if (direction == 1) {
					//selectedWall.Build = false;
					cell = selectedWall.ParentCell;

					// Check if the cell in that direction is not already part of maze
					if (cell.i < _size - 1 && !maze[cell.i + 1, cell.j].PartOfMaze) {

						cell.Right.Build = false;

						// Add all the walls of the neighboring cell
						otherCell = maze[cell.i + 1, cell.j];
						otherCell.Left.Build = false;

						//walls.Add(cell.Left);
						walls.Add(otherCell.Right);
						walls.Add(otherCell.Top);
						walls.Add(otherCell.Bottom);

						otherCell.PartOfMaze = true;
						added = true;
					}
				} else if (direction == 2) {
					//selectedWall.Build = false;
					cell = selectedWall.ParentCell;

					// Check if the cell in that direction is not already part of maze
					if (cell.j > 0 && !maze[cell.i, cell.j - 1].PartOfMaze) {

						cell.Top.Build = false;

						// Add all the walls of the neighboring cell
						otherCell = maze[cell.i, cell.j - 1];
						otherCell.Bottom.Build = false;

						walls.Add(otherCell.Left);
						walls.Add(otherCell.Right);
						walls.Add(otherCell.Top);
						//walls.Add(otherCell.Bottom);

						otherCell.PartOfMaze = true;
						added = true;
					}
				} else if (direction == 3) {
					//selectedWall.Build = false;
					cell = selectedWall.ParentCell;

					// Check if the cell in that direction is not already part of maze
					if (cell.j < _size - 1 && !maze[cell.i, cell.j + 1].PartOfMaze) {

						cell.Bottom.Build = false;

						// Add all the walls of the neighboring cell
						otherCell = maze[cell.i, cell.j + 1];
						otherCell.Top.Build = false;

						walls.Add(otherCell.Left);
						walls.Add(otherCell.Right);
						//walls.Add(otherCell.Top);
						walls.Add(otherCell.Bottom);

						otherCell.PartOfMaze = true;
						added = true;
					}
				}

				// Remove the current selected wall
				try {
					walls.RemoveAt(ran);
				} catch (Exception e) {
					
				}				

				if (ShowGeneration && added) {
					_painter.Paint(maze, otherCell.i, otherCell.j);

					//Make the thread sleep so that the user can see the move taken
					Thread.Sleep(50);
				}
			}

			if (!ShowGeneration) {
				_painter.CleanPaint(maze);
			}
			
			Console.WriteLine("Generated Maze");
		}
	}	
}

namespace Objects {
	public class Cell {
		/// <summary>
		/// Left wall enabled
		/// </summary>
		public Wall Left { get; set; }
		/// <summary>
		/// Right wall enabled
		/// </summary>
		public Wall Right { get; set; }
		/// <summary>
		/// Top wall enabled
		/// </summary>
		public Wall Top { get; set; }
		/// <summary>
		/// Bottom wall enabled
		/// </summary>
		public Wall Bottom { get; set; }

		public bool PartOfMaze { get; set; }

		public int i { get; }
		public int j { get; }	

		/// <summary>
		/// Creates a new maze cell with walls on all sides
		/// </summary>
		public Cell(int i, int j) {
			this.i = i;
			this.j = j;

			PartOfMaze = false;

			Left = new Wall(this);
			Right = new Wall(this);
			Top = new Wall(this);
			Bottom = new Wall(this);
		}		
	}

	public class Wall {

		public bool Build { get; set; }
		public Cell ParentCell { get; }

		public Wall(Cell parentCell) {
			ParentCell = parentCell;
			Build = true;	
		}
	}
}
