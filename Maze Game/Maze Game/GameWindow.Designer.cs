﻿namespace Maze_Game {
	partial class GameWindow {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.GamePanel = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
			this.GamePanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// GamePanel
			// 
			this.GamePanel.BackColor = System.Drawing.Color.White;
			this.GamePanel.Controls.Add(this.button1);
			this.GamePanel.Controls.Add(this.checkBox1);
			this.GamePanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GamePanel.Location = new System.Drawing.Point(0, 0);
			this.GamePanel.Margin = new System.Windows.Forms.Padding(0);
			this.GamePanel.Name = "GamePanel";
			this.GamePanel.Size = new System.Drawing.Size(1000, 541);
			this.GamePanel.TabIndex = 2;
			this.GamePanel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GamePanel_MouseClick);
			this.GamePanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GamePanel_MouseDown);
			this.GamePanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.GamePanel_MouseMove);
			this.GamePanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.GamePanel_MouseUp);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(851, 35);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(137, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "Generate Maze";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// checkBox1
			// 
			this.checkBox1.AutoSize = true;
			this.checkBox1.Location = new System.Drawing.Point(851, 12);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(137, 17);
			this.checkBox1.TabIndex = 0;
			this.checkBox1.Text = "Show Maze Generation";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// backgroundWorker1
			// 
			this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
			// 
			// GameWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(1000, 541);
			this.Controls.Add(this.GamePanel);
			this.Name = "GameWindow";
			this.Text = "Maze Game";
			this.Shown += new System.EventHandler(this.GameWindow_Load);
			this.GamePanel.ResumeLayout(false);
			this.GamePanel.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Panel GamePanel;
		private System.ComponentModel.BackgroundWorker backgroundWorker1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.CheckBox checkBox1;
	}
}

